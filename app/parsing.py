from bs4 import BeautifulSoup
from urllib.request import urlopen
 
html_doc = urlopen('http://habrahabr.ru/post/193242/').read()
soup = BeautifulSoup(html_doc)

#comments = soup.find('div', 'comments_list')
#print(type(comments))

for comment in soup.find_all('div', 'comment_item'):

    result = {}

    result['score']      = comment.find('span','score').text
    result['data']       = comment.find('time').text
    result['user_name']  = comment.find('a','username').text
    result['id_comment'] = comment['id'][8:]
    result['parent_id']  = comment.find('span','parent_id')['data-parent_id']
    result['message']    = comment.find('div','message').text

    print (result)